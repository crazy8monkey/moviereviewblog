-- MySQL dump 10.13  Distrib 8.0.16, for macos10.14 (x86_64)
--
-- Host: localhost    Database: movie_reviews
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `reviews` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `review_title` longtext,
  `body` longtext,
  `is_public` int(11) NOT NULL,
  `date_published` longtext,
  `seo_url` longtext,
  `acting` int(11) NOT NULL,
  `writing` int(11) NOT NULL,
  `cast` int(11) NOT NULL,
  `creativity` int(11) NOT NULL,
  PRIMARY KEY (`review_id`),
  UNIQUE KEY `review_id` (`review_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,'fsdfa','<p>&lt;p&gt;fsdfsdafs&lt;/p&gt;</p>',0,NULL,'fsdfa',0,0,0,0),(2,'fsdfsd','<p>&lt;p&gt;fdsfdsf&lt;/p&gt;</p>',0,NULL,'fsdfsd',0,0,0,0),(3,'sdfsdf','<p>&lt;p&gt;fsdffsda&lt;/p&gt;</p>',0,NULL,'sdfsdf',0,0,0,0),(4,'ytytryre','&lt;p&gt;vcxvxzvc&lt;/p&gt;',0,NULL,'ytytryre',0,0,0,0),(5,'htjhjtjghjghfj','&lt;p&gt;fdsfsadfdsa&lt;/p&gt;',0,NULL,'htjhjtjghjghfj',0,0,0,0),(6,'gfhgfhfgdhfgdh','&lt;p&gt;gfdgfdg&lt;/p&gt;',0,NULL,'gfhgfhfgdhfgdh',0,0,0,0),(7,'fdsffsafsa','<p>&lt;p&gt;fdsfdsfsafas&lt;/p&gt;</p>',0,NULL,'fdsffsafsa',0,0,0,0),(8,'thgjgjkljg','<p>&lt;p&gt;fdsfdsafsad&lt;/p&gt;</p>',0,NULL,'thgjgjkljg',0,0,0,0);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-28 18:53:28
