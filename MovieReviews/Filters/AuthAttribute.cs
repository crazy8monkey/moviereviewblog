﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace MovieReviews.AuthData
{
    public class AuthAttribute: ActionFilterAttribute, IAuthenticationFilter
    {
        private bool _auth;

        public AuthAttribute()
        {
        }

        public void OnAuthentication(AuthenticationContext filterContext)
        {
            throw new NotImplementedException();
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            throw new NotImplementedException();
        }
    }
}
