﻿using System;
using System.Web;
using System.Web.Mvc;

namespace MovieReviews.Filters
{
    public class UserAuthAttribute : AuthorizeAttribute
    {
        protected virtual UserPriciple CurrentUser
        {
            get { return HttpContext.Current.User as UserPriciple; }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return (CurrentUser != null) ? true: false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            RedirectToRouteResult routeData = null;

            if (CurrentUser == null)
            {
                routeData = new RedirectToRouteResult
                    (new System.Web.Routing.RouteValueDictionary
                    (new
                    {
                        controller = "Admin",
                        action = "Index",
                    }
                    ));
            }
            
            filterContext.Result = routeData;
        }   
    }
}
