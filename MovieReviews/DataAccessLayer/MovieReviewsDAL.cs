﻿using System;
using System.Data.Entity;
using MovieReviews.Models;

namespace MovieReviews.DataAccessLayer
{
    public class MovieReviewsDAL : DbContext
    {
        public DbSet<CategoryModel> Category { get; set; }
        public DbSet<ReviewModel> Review { get; set; }
        public DbSet<ReviewCategoryModel> ReviewCategory { get; set; }
        public DbSet<UserModel> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<MovieReviewsDAL>(null);
            modelBuilder.Entity<CategoryModel>().ToTable("categories");
            modelBuilder.Entity<ReviewModel>().ToTable("reviews");
            modelBuilder.Entity<ReviewCategoryModel>().ToTable("review_categories");
            modelBuilder.Entity<UserModel>().ToTable("users");
            base.OnModelCreating(modelBuilder);
        }
    }
}
