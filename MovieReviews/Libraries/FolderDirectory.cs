﻿using System;
using System.IO;

namespace MovieReviews.Libraries
{
    public class FolderDirectory
    {
        private string DirectoryPath = @"images/";

        public void CreateDirectory(string FolderName)
        {
            if (!Directory.Exists(String.Format("{0}{1}", DirectoryPath, FolderName)))
            {
                Directory.CreateDirectory(String.Format("{0}{1}", DirectoryPath, FolderName));
            }
        }

        public void DeleteDiretory(string FolderName)
        {
            var directory = new DirectoryInfo(String.Format("{0}{1}", DirectoryPath, FolderName));
            directory.Delete(true);
        }
    }
}
