﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MovieReviews.BusinessLayer;
using MovieReviews.Models;
using MovieReviews.ViewModels;

namespace MovieReviews.Controllers
{
    public class CategoryController : Controller
    {
        private ReviewBusinessLayer reviewBusinessLayer = new ReviewBusinessLayer();
        private CategoryBusinessLayer categoryBusinessLayer = new CategoryBusinessLayer();

        public ActionResult Index(string seoUrl)
        {
            var currentCategory = categoryBusinessLayer.GetCategoryByUrl(seoUrl);

            //meta data
            ViewBag.Title = String.Format("Movie Review : Viewing Category : {0}", currentCategory.category_name);
            ViewBag.MetaDescription = "This is a web blog of Adam Schmidt, who is a movie fanatic and decided to give his own opinion and rants of the movies he watches";
            ViewBag.Canonical = String.Format("https://www.websitename.com/category/{0}", currentCategory.category_seo_url);

            ViewBag.NavigationSchema = string.Join(",", GenerateSchemaNavigation());

            //UI display data
            List<ReviewModel> categtoryReviews = reviewBusinessLayer.ReviewsByCategory(currentCategory.category_id);
            List<ReviewsViewModel> reviews = new List<ReviewsViewModel>();
            ReviewsListViewModel reviewList = new ReviewsListViewModel();
            
            foreach (ReviewModel item in categtoryReviews)
            {
                ReviewsViewModel review = new ReviewsViewModel();
                if (item.is_public == 1)
                {
                    review.review_id = item.review_id;
                    review.review_title = item.review_title;
                    review.seo_url = item.seo_url;
                    review.movie_image = item.movie_image;
                    review.schema = movieReviewSchema(item);
                    List<CategoryViewModel> reviewCategories = new List<CategoryViewModel>();

                    foreach (ReviewCategoryModel categoryItem in item.Categories)
                    {
                        CategoryViewModel category = new CategoryViewModel();
                        category.category_id = categoryItem.category_id;
                        category.category_name = categoryItem.category.category_name;
                        category.category_seo_url = categoryItem.category.category_seo_url;
                        reviewCategories.Add(category);
                    }

                    review.categories = reviewCategories;
                    reviews.Add(review);
                }

                //schema
            }
            reviewList.reviews = reviews;
            return View(reviewList);
            //return View();
        }

        public List<string> GenerateSchemaNavigation()
        {
            List<string> navigationSchema = new List<string>();

            Schema.NET.SiteNavigationElement homeNavigation = new Schema.NET.SiteNavigationElement()
            {
                Name = "Movie Reviews Home",
                Url = new Uri("https://www.websitename.com")
            };

            Schema.NET.SiteNavigationElement aboutNavigation = new Schema.NET.SiteNavigationElement()
            {
                Name = "Moview Reviews About",
                Url = new Uri("https://www.websitename.com/about")
            };

            Schema.NET.SiteNavigationElement contactNavigation = new Schema.NET.SiteNavigationElement()
            {
                Name = "Moview Reviews Contact",
                Url = new Uri("https://www.websitename.com/contact")
            };

            Schema.NET.SiteNavigationElement privacyPolicyNavigation = new Schema.NET.SiteNavigationElement()
            {
                Name = "Moview Reviews Privacy Policy",
                Url = new Uri("https://www.websitename.com/privacy-policy")
            };

            navigationSchema.Add(homeNavigation.ToString());
            navigationSchema.Add(aboutNavigation.ToString());
            navigationSchema.Add(contactNavigation.ToString());
            navigationSchema.Add(privacyPolicyNavigation.ToString());

            return navigationSchema;
        }

        private string movieReviewSchema(ReviewModel movie)
        {
            Schema.NET.Review schema = new Schema.NET.Review()
            {
                Name = String.Format("{0} Review", movie.review_title),
                Author = new Schema.NET.Person() { Name = "Adam Schmidt" },
                ReviewRating = new Schema.NET.Rating()
                {
                    RatingValue = (movie.acting + movie.cast + movie.creativity + movie.writing) / 4
                },
                DateCreated = DateTimeOffset.Parse(movie.date_published),
                ItemReviewed = new Schema.NET.Thing()
                {
                    Name = movie.review_title,
                    SubjectOf = new Schema.NET.Movie()
                    {
                        Name = movie.review_title,
                        Director = new Schema.NET.Person()
                        {
                            Name = movie.movie_director
                        },
                        DateCreated = DateTimeOffset.Parse(movie.movie_released),
                        Image = new Schema.NET.ImageObject()
                        {
                            Url = new Uri(String.Format("https://www.websitename.com/images/{0}/{1}", movie.seo_url, movie.movie_image))
                        }
                    }
                }
            };

            return schema.ToString();
        }
    }
}
