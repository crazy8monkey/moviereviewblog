﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MovieReviews.BusinessLayer;
using MovieReviews.Models;
using MovieReviews.ViewModels;
using Newtonsoft.Json;
using MovieReviews.Filters;

namespace MovieReviews.Controllers
{
    public class AdminController : Controller
    {
        private CategoryBusinessLayer categoryBusinessLayer = new CategoryBusinessLayer();
        private ReviewBusinessLayer reviewBusinessLayer = new ReviewBusinessLayer();
        private UserBusinessLayer userBusinessLayer = new UserBusinessLayer();

        public ActionResult Index()
        {
            ViewBag.Title = "Admin: Login";
            ViewBag.ShowBar = false;
            return View();
        }

        [UserAuth]
        public ActionResult Dashboard()
        {
            ViewBag.Title = "Admin: Dashboard";
            ViewBag.ShowBar = true;
            return View();
        }

        [UserAuth]
        public ActionResult Reviews()
        {
            ViewBag.Title = "Admin: Reviews list";
            ViewBag.ShowBar = true;
            List<ReviewModel> currentReviewsList = reviewBusinessLayer.Reviews();
            List<ReviewsViewModel> reviews = new List<ReviewsViewModel>();
            ReviewsListViewModel reviewList = new ReviewsListViewModel();

            foreach (ReviewModel item in currentReviewsList)
            {
                ReviewsViewModel review = new ReviewsViewModel();
                review.review_id = item.review_id;
                review.review_title = item.review_title;

                reviews.Add(review);
            }

            reviewList.reviews = reviews;

            return View(reviewList);
        }

        [UserAuth]
        public ActionResult EditReview(int id)
        {
            ViewBag.ShowBar = true;
            ReviewModel currentReview = reviewBusinessLayer.GetReview(id);
            
            List<CategoryModel> currentCategoryList = categoryBusinessLayer.CategoryList();
            List<CategoryViewModel> categories = new List<CategoryViewModel>();
            ReviewPostEditorViewModel reviewPostEditorViewModel = new ReviewPostEditorViewModel();
            List<ReviewCategoryViewModel> reviewCategories = new List<ReviewCategoryViewModel>();
            reviewPostEditorViewModel.review_id = id;
            reviewPostEditorViewModel.review_title = currentReview.review_title;
            reviewPostEditorViewModel.body = currentReview.body;
            reviewPostEditorViewModel.is_public = currentReview.is_public;
            ViewBag.Title = String.Format("Admin: Edit Review {0}", currentReview.review_title);

            List<int> categoryids = new List<int>();

            foreach (ReviewCategoryModel item in currentReview.Categories)
            {
                categoryids.Add(item.category_id);
            }

            reviewPostEditorViewModel.acting = currentReview.acting;
            reviewPostEditorViewModel.cast = currentReview.cast;
            reviewPostEditorViewModel.creativity = currentReview.creativity;
            reviewPostEditorViewModel.writing = currentReview.writing;
            reviewPostEditorViewModel.movie_director = currentReview.movie_director;
            reviewPostEditorViewModel.movie_released = currentReview.movie_released;
            reviewPostEditorViewModel.seo_url = currentReview.seo_url;
            reviewPostEditorViewModel.movie_image = currentReview.movie_image;
            

            foreach (CategoryModel item in currentCategoryList)
            {
                CategoryViewModel category = new CategoryViewModel();
                category.category_id = item.category_id;
                category.category_name = item.category_name;
                bool categoryCheck = categoryids.IndexOf(item.category_id) != -1;
                category.category_checked = categoryCheck;
                categories.Add(category);
            }

            reviewPostEditorViewModel.CategoryList = categories;

            return View(reviewPostEditorViewModel);
        }

        [UserAuth]
        public ActionResult AddReview()
        {
            ViewBag.Title = "Admin: Add Review";
            ViewBag.ShowBar = true;
            List<CategoryModel> currentCategoryList = categoryBusinessLayer.CategoryList();
            List<CategoryViewModel> categories = new List<CategoryViewModel>();
            ReviewPostEditorViewModel reviewPostEditorViewModel = new ReviewPostEditorViewModel();

            foreach (CategoryModel item in currentCategoryList)
            {
                CategoryViewModel category = new CategoryViewModel();
                category.category_id = item.category_id;
                category.category_name = item.category_name;

                categories.Add(category);
            }
            reviewPostEditorViewModel.CategoryList = categories;
            return View(reviewPostEditorViewModel);
        }

        [UserAuth]
        public ActionResult Settings()
        {
            ViewBag.Title = "Admin: Settings";
            ViewBag.ShowBar = true;
            List<CategoryModel> currentCategoryList = categoryBusinessLayer.CategoryList();
            List<CategoryViewModel> categories = new List<CategoryViewModel>();
            CategoryListViewModel categoryListViewModel = new CategoryListViewModel();

            foreach (CategoryModel item in currentCategoryList)
            {
                CategoryViewModel category = new CategoryViewModel();
                category.category_id = item.category_id;
                category.category_name = item.category_name;

                categories.Add(category);
            }
            categoryListViewModel.CategoryList = categories;
            return View(categoryListViewModel);
        }

        [UserAuth]
        public ActionResult EditCategory(int id)
        {
            var currentCategory = categoryBusinessLayer.GetCategory(id);
            CategoryViewModel category = new CategoryViewModel();
            category.category_id = id;
            category.category_name = currentCategory.category_name;

            return View(category);
        }

        public ActionResult PostEditor(int id)
        {
            return View();
        }

        public ActionResult CreateUserTest()
        {
            var salt = userBusinessLayer.CreateSalt();
            var hashedPassword = userBusinessLayer.GenerateSHA256Hash("Crazy3@monkey", salt);
            ViewBag.Salt = salt;
            ViewBag.Password = hashedPassword;
            ViewBag.UserLoginCheck = userBusinessLayer.CheckCredentials("Crazy3@monkey", hashedPassword, salt);
            return View();
        }

        [UserAuth]
        public ActionResult LogOut()
        {
            HttpCookie cookie = new HttpCookie("User", "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie);

            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "admin");
        }

        [HttpPost]
        public ActionResult AddCategory(CategoryModel category)
        {
            categoryBusinessLayer.NewCategory(category);
            return RedirectToAction("Settings", "admin");
        }

        [UserAuth]
        public ActionResult RemoveCategory(int id)
        {
            categoryBusinessLayer.RemoveCategory(id);
            return RedirectToAction("Settings", "admin");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SaveNewReview(ReviewPostModel review)
        {
            var newReview = reviewBusinessLayer.NewReview(review);
            return RedirectToAction("EditReview", "admin", new { id = newReview.review_id });
        }


        [HttpPost, ValidateInput(false)]
        public ActionResult EditReviewPost(ReviewPostModel review)
        {
            reviewBusinessLayer.EditReview(review);
            return RedirectToAction("EditReview", "admin", new { id = review.review_id });
        }

        [UserAuth]
        public ActionResult RemoveReview(int id)
        {
            reviewBusinessLayer.Remove(id);
            return RedirectToAction("Reviews", "admin");
        }

        public ActionResult UpdateCategory(CategoryModel category)
        {
            categoryBusinessLayer.UpdateCategory(category);
            return RedirectToAction("Settings", "admin");
        }

        [HttpPost]
        public ActionResult AuthorizeUser(UserModel user)
        {
            Console.WriteLine(String.Format("Email Submitted {0}", user.email));
            Console.WriteLine(String.Format("Password Submitted {0}", user.password));
            var userLoginCheck = userBusinessLayer.CheckUser(user.email);

            Console.WriteLine(String.Format("userLoginCheck  {0}", userLoginCheck.password));
            if (userLoginCheck == null)
            {
                return RedirectToAction("Index", "admin");
            } else {
                var passwordCheck = userBusinessLayer.CheckCredentials(user.password, userLoginCheck.password, userLoginCheck.salt);
                if(passwordCheck)
                {
                    string userData = JsonConvert.SerializeObject(user);
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, user.email, DateTime.Now, DateTime.Now.AddMinutes(30), false, userData);
                    string enTicket = FormsAuthentication.Encrypt(authTicket);
                    HttpCookie faCookie = new HttpCookie("User", enTicket);
                    Response.Cookies.Add(faCookie);
                    return RedirectToAction("Dashboard", "admin");
                } else
                {
                    return RedirectToAction("Index", "admin");
                }
            }
        }
    }
}
