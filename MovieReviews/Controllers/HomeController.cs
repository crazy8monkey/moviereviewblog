﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using MovieReviews.BusinessLayer;
using MovieReviews.Models;
using MovieReviews.ViewModels;

namespace MovieReviews.Controllers
{
    public class HomeController : Controller
    {
        private ReviewBusinessLayer reviewBusinessLayer = new ReviewBusinessLayer();

        public ActionResult Index()
        {
            //meta data
            ViewBag.Title = "Movie Review: Home";
            ViewBag.MetaDescription = "This is a web blog of Adam Schmidt, who is a movie fanatic and decided to give his own opinion and rants of the movies he watches";
            ViewBag.Canonical = "https://www.websitename.com";

            ViewBag.NavigationSchema = string.Join(",", GenerateSchemaNavigation());


            //UI display data
            List<ReviewModel> currentReviewsList = reviewBusinessLayer.Reviews();
            List<ReviewsViewModel> reviews = new List<ReviewsViewModel>();
            ReviewsListViewModel reviewList = new ReviewsListViewModel();
            List<CategoryViewModel> allCategories = new List<CategoryViewModel>();
            //List<Schema.NET.Movie> movieSchema = new List<Schema.NET.Movie>();

            foreach (ReviewModel item in currentReviewsList)
            {
                ReviewsViewModel review = new ReviewsViewModel();
                if (item.is_public == 1)
                {

                    review.review_id = item.review_id;
                    review.review_title = item.review_title;
                    review.seo_url = item.seo_url;
                    review.movie_image = item.movie_image;
                    review.schema = movieReviewSchema(item);
                    List<CategoryViewModel> reviewCategories = new List<CategoryViewModel>();

                    foreach(ReviewCategoryModel categoryItem in item.Categories)
                    {
                        CategoryViewModel category = new CategoryViewModel();
                        category.category_id = categoryItem.category_id;
                        category.category_name = categoryItem.category.category_name;
                        category.category_seo_url = categoryItem.category.category_seo_url;
                        reviewCategories.Add(category);

                        if (!allCategories.Any(c => c.category_id == categoryItem.category_id))
                        {
                            allCategories.Add(category);
                        }
                    }

                    review.categories = reviewCategories;
                    reviews.Add(review);
                }

                //schema
            }
            ViewBag.Categories = allCategories;
            reviewList.reviews = reviews;
            return View(reviewList);
        }

        [HandleError]
        public ActionResult Movie(string seoUrl)
        {
            ReviewModel currentReivew = reviewBusinessLayer.GetReviewBySEO(seoUrl);
            if(currentReivew.is_public == 0 || currentReivew == null)
            {
                //throw new HttpException(404, "File Not Found");
                return HttpNotFound();
            }


            //meta data
            ViewBag.Title = String.Format("Reading Review - {0}", currentReivew.review_title);
            ViewBag.MetaDescription = String.Format("Moview Review for {0}", currentReivew.review_title);
            ViewBag.Canonical = String.Format("https://www.websitename.com/{0}", currentReivew.review_title);

            ViewBag.NavigationSchema = string.Join(",", GenerateSchemaNavigation());

            //UI Data
            List<CategoryViewModel> reviewCategories = new List<CategoryViewModel>();
            List<string> categories = new List<string>(); 
            ReviewsViewModel reviewViewModel = new ReviewsViewModel();
            ViewBag.Title = currentReivew.review_title;

            reviewViewModel.review_title = currentReivew.review_title;
            reviewViewModel.body = currentReivew.body;
            

            foreach (ReviewCategoryModel categoryItem in currentReivew.Categories)
            {
                categories.Add(categoryItem.category.category_name);
            }

            //var result = string.Join(",", arr);

            reviewViewModel.acting = currentReivew.acting;
            reviewViewModel.cast = currentReivew.cast;
            reviewViewModel.creativity = currentReivew.creativity;
            reviewViewModel.writing = currentReivew.writing;
            reviewViewModel.categories_string = string.Join(" / ", categories.ToArray());
            reviewViewModel.categories = reviewCategories;
            reviewViewModel.seo_url = currentReivew.seo_url;
            reviewViewModel.movie_image = currentReivew.movie_image;


            reviewViewModel.schema = movieReviewSchema(currentReivew);

            return View(reviewViewModel);
        }

        public ActionResult About()
        {
            ViewBag.Title = "Movie Review: About";
            ViewBag.MetaDescription = "This is a web blog of Adam Schmidt, who is a movie fanatic and decided to give his own opinion and rants of the movies he watches";
            ViewBag.Canonical = "https://www.websitename.com/about";

            ViewBag.NavigationSchema = string.Join(",", GenerateSchemaNavigation());
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Title = "Movie Review: About";
            ViewBag.MetaDescription = "This is a web blog of Adam Schmidt, who is a movie fanatic and decided to give his own opinion and rants of the movies he watches";
            ViewBag.Canonical = "https://www.websitename.com/contact";

            ViewBag.NavigationSchema = string.Join(",", GenerateSchemaNavigation());
            return View();
        }

        public ActionResult PrivacyPolicy()
        {
            ViewBag.Title = "Movie Review: Privacy Policy";
            ViewBag.MetaDescription = "This is a web blog of Adam Schmidt, who is a movie fanatic and decided to give his own opinion and rants of the movies he watches";
            ViewBag.Canonical = "https://www.websitename.com/privacy-policy";

            ViewBag.NavigationSchema = string.Join(",", GenerateSchemaNavigation());
            return View();
        }

        private List<string> GenerateSchemaNavigation()
        {
            List<string> navigationSchema = new List<string>();

            Schema.NET.SiteNavigationElement homeNavigation = new Schema.NET.SiteNavigationElement()
            {
                Name = "Movie Reviews Home",
                Url = new Uri("https://www.websitename.com")
            };

            Schema.NET.SiteNavigationElement aboutNavigation = new Schema.NET.SiteNavigationElement()
            {
                Name = "Moview Reviews About",
                Url = new Uri("https://www.websitename.com/about")
            };

            Schema.NET.SiteNavigationElement contactNavigation = new Schema.NET.SiteNavigationElement()
            {
                Name = "Moview Reviews Contact",
                Url = new Uri("https://www.websitename.com/contact")
            };

            Schema.NET.SiteNavigationElement privacyPolicyNavigation = new Schema.NET.SiteNavigationElement()
            {
                Name = "Moview Reviews Privacy Policy",
                Url = new Uri("https://www.websitename.com/privacy-policy")
            };

            navigationSchema.Add(homeNavigation.ToString());
            navigationSchema.Add(aboutNavigation.ToString());
            navigationSchema.Add(contactNavigation.ToString());
            navigationSchema.Add(privacyPolicyNavigation.ToString());

            return navigationSchema;
        }

        private string movieReviewSchema(ReviewModel movie)
        {
            Schema.NET.Review schema = new Schema.NET.Review()
            {
                Name = String.Format("{0} Review", movie.review_title),
                Author = new Schema.NET.Person() { Name = "Adam Schmidt" },
                ReviewRating = new Schema.NET.Rating()
                {
                    RatingValue = (movie.acting + movie.cast + movie.creativity + movie.writing) / 4
                },
                DateCreated = DateTimeOffset.Parse(movie.date_published),
                ItemReviewed = new Schema.NET.Thing()
                {
                    Name = movie.review_title,
                    SubjectOf = new Schema.NET.Movie()
                    {
                        Name = movie.review_title,
                        Director = new Schema.NET.Person()
                        {
                            Name = movie.movie_director
                        },
                        DateCreated = DateTimeOffset.Parse(movie.movie_released),
                        Image = new Schema.NET.ImageObject()
                        {
                            Url = new Uri(String.Format("https://www.websitename.com/images/{0}/{1}", movie.seo_url, movie.movie_image))
                        }
                    }
                }
            };

            return schema.ToString();
        }
        
    }
}
