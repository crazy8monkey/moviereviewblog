﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MovieReviews
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");



            routes.MapRoute(
                name: "Homepage",
                url: "",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "About",
                url: "about",
                defaults: new { controller = "Home", action = "About" }
            );

            routes.MapRoute(
                name: "PrivacyPolicy",
                url: "privacy-policy",
                defaults: new { controller = "Home", action = "PrivacyPolicy" }
            );

            routes.MapRoute(
                name: "Contact",
                url: "contact",
                defaults: new { controller = "Home", action = "Contact" }
            );

            

            routes.MapRoute(
                name: "AdminLogin",
                url: "admin",
                defaults: new { controller = "Admin", action = "Index"}
            );

            routes.MapRoute(
                name: "AdminDashboard",
                url: "admin/dashboard",
                defaults: new { controller = "Admin", action = "Dashboard" }
            );

            routes.MapRoute(
                name: "AdminReviews",
                url: "admin/reviews",
                defaults: new { controller = "Admin", action = "Reviews" }
            );

            routes.MapRoute(
                name: "AdminEditReview",
                url: "admin/edit-review/{id}",
                defaults: new { controller = "Admin", action = "EditReview", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "AdminAddReview",
                url: "admin/add-review",
                defaults: new { controller = "Admin", action = "AddReview"}
            );

            routes.MapRoute(
                name: "AdminSettings",
                url: "admin/settings",
                defaults: new { controller = "Admin", action = "Settings" }
            );

            routes.MapRoute(
                name: "Review",
                url: "{seoUrl}",
                defaults: new { controller = "Home", action = "Movie", seoUrl = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "CategoryPage",
                url: "category/{seoUrl}",
                defaults: new { controller = "Category", action = "Index", seoUrl = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


        }
    }
}
