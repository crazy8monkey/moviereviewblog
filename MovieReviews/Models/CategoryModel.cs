﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MovieReviews.Models
{
    public class CategoryModel
    {
        [Key]
        public int category_id { get; set; }
        public string category_name { get; set; }
        public string category_seo_url { get; set; }
    }
}
