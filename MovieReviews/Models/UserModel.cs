﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MovieReviews.Models
{
    public class UserModel
    {
        [Key]
        public int user_id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string salt { get; set; }
    }
}
