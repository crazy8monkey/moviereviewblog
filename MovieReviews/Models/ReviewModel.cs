﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System.Web.Mvc;

namespace MovieReviews.Models
{
    public class ReviewModel
    {
        [Key]
        public int review_id { get; set; }
        public string review_title { get; set; }
        [AllowHtml]
        public string body { get; set; }
        public int is_public { get; set; }
        public string date_published { get; set; }
        public string seo_url { get; set; }
        public List<int> category_id { get; set; }
        public int acting { get; set; }
        public int writing { get; set; }
        public int cast { get; set; }
        public int creativity { get; set; }
        public string movie_director { get; set; }
        public string movie_released { get; set; }
        public string movie_image { get; set; }

        [ForeignKey("review_id")]
        public virtual List<ReviewCategoryModel> Categories { get; set; }
    }
}
