﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieReviews.Models
{
    public class ReviewTypeModel
    {
        [Key]
        [Column("review_id", Order = 1)]
        public int review_id { get; set; }

        [Key]
        [Column("type", Order = 2)]
        public string type { get; set; }

        public int rating { get; set; }
    }
}
