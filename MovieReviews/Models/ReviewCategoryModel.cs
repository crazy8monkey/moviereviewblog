﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieReviews.Models
{
    public class ReviewCategoryModel
    {
        [Key]
        [Column("review_id", Order = 1)]
        public int review_id { get; set; }

        [Key]
        [Column("category_id", Order = 2)]
        public int category_id { get; set; }

        [ForeignKey("category_id")]
        public virtual CategoryModel category { get; set; }
    }
}
