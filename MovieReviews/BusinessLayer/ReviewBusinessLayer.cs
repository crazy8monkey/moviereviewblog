﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using MovieReviews.DataAccessLayer;
using MovieReviews.Libraries;
using MovieReviews.Models;

namespace MovieReviews.BusinessLayer
{
    public class ReviewBusinessLayer
    {
        private MovieReviewsDAL movieReviewsDAL = new MovieReviewsDAL();
        private CategoryBusinessLayer categoryBusinessLayer = new CategoryBusinessLayer();
        private FolderDirectory folderDirectory = new FolderDirectory();

        public List<ReviewModel> Reviews()
        {
            return movieReviewsDAL.Review.ToList();
        }

        public ReviewModel GetReview(int id)
        {
            return movieReviewsDAL.Review.Find(id);
        }

        public ReviewModel GetReviewBySEO(string seoUrl)
        {
            return movieReviewsDAL.Review.Where(r => r.seo_url == seoUrl).FirstOrDefault();
        }

        public List<ReviewModel> ReviewsByCategory(int category_id)
        {
            List<ReviewModel> reviews = new List<ReviewModel>();
            foreach (ReviewModel item in movieReviewsDAL.Review.ToList())
            {
                var categoryIdMatch = item.Categories.Where(c => c.category_id == category_id);
                if (categoryIdMatch.Count() > 0 && item.is_public == 1) 
                {
                    reviews.Add(item);
                }
            }
            return reviews;
        }

        public ReviewModel NewReview(ReviewPostModel review)
        {
            var reveiwTitle = seoUrl(review.review_title);
            folderDirectory.CreateDirectory(reveiwTitle);

            var movieImageFileName = Path.GetFileName(review.movie_image.FileName);
            var fileNameExtension = Path.GetExtension(review.movie_image.FileName);
            var path = Path.Combine(HttpContext.Current.Server.MapPath(string.Format("~/images/{0}/", reveiwTitle)), String.Format("{0}{1}", reveiwTitle, fileNameExtension));
            //review.movie_image.RenameUploadFile(file)
            review.movie_image.SaveAs(path);

            ReviewModel newReview = new ReviewModel();
            newReview.review_title = review.review_title;
            newReview.body = HttpContext.Current.Server.HtmlEncode(review.body);
            newReview.is_public = 0;
            newReview.seo_url = reveiwTitle;
            newReview.acting = review.acting;
            newReview.writing = review.writing;
            newReview.cast = review.cast;
            newReview.creativity = review.creativity;
            newReview.movie_director = review.movie_director;
            newReview.movie_released = review.movie_released;
            newReview.movie_image = String.Format("{0}{1}", reveiwTitle, fileNameExtension);


            movieReviewsDAL.Review.Add(newReview);
            movieReviewsDAL.SaveChanges();
            foreach(var category in review.category_id)
            {
                movieReviewsDAL.ReviewCategory.Add(new ReviewCategoryModel
                {
                    review_id = newReview.review_id,
                    category_id = category
                });
                movieReviewsDAL.SaveChanges();
            }

            movieReviewsDAL.SaveChanges();
            
            return newReview;
        }

        public ReviewModel EditReview(ReviewPostModel review)
        {
            var currentReview = movieReviewsDAL.Review.FirstOrDefault(r => r.review_id == review.review_id);
            if (review.movie_image != null)
            {
                var currentFile = HttpContext.Current.Server.MapPath(String.Format("~/images/{0}/{1}", currentReview.seo_url, currentReview.movie_image));
                File.Delete(currentFile);

                var fileNameExtension = Path.GetExtension(review.movie_image.FileName);
                var newImage = Path.Combine(HttpContext.Current.Server.MapPath(string.Format("~/images/{0}/", currentReview.seo_url)), String.Format("{0}{1}", currentReview.seo_url, fileNameExtension));

                review.movie_image.SaveAs(newImage);
                currentReview.movie_image = String.Format("{0}{1}", currentReview.seo_url, fileNameExtension);

            }

            var reviewCategories = movieReviewsDAL.ReviewCategory.Where(r => r.review_id == review.review_id).ToList();
            currentReview.review_title = review.review_title;
            currentReview.body = review.body;
            currentReview.is_public = review.is_public;
            currentReview.seo_url = seoUrl(review.review_title);
            currentReview.acting = review.acting;
            currentReview.writing = review.writing;
            currentReview.cast = review.cast;
            currentReview.creativity = review.creativity;
            currentReview.movie_director = review.movie_director;
            currentReview.movie_released = review.movie_released;

            if (currentReview.date_published == null && review.is_public == 1)
            {
                currentReview.date_published = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            }

            

                //removing old categories
                foreach (var category in reviewCategories)
            {
                movieReviewsDAL.ReviewCategory.Remove(category);
                movieReviewsDAL.SaveChanges();
            }

            //adding new categories
            foreach (var category in review.category_id)
            {
                movieReviewsDAL.ReviewCategory.Add(new ReviewCategoryModel
                {
                    review_id = currentReview.review_id,
                    category_id = category
                });
                movieReviewsDAL.SaveChanges();
            }

            
            movieReviewsDAL.SaveChanges();
            return currentReview;
        }

        public void Remove(int review_id)
        {
            
            var ReviewToRemove = movieReviewsDAL.Review.FirstOrDefault(r => r.review_id == review_id);
            folderDirectory.DeleteDiretory(ReviewToRemove.seo_url);
            var reviewCategories = movieReviewsDAL.ReviewCategory.Where(r => r.review_id == review_id).ToList();
            foreach(var category in reviewCategories)
            {
                movieReviewsDAL.ReviewCategory.Remove(category);
                movieReviewsDAL.SaveChanges();
            }
    
            movieReviewsDAL.Review.Remove(ReviewToRemove);
            movieReviewsDAL.SaveChanges();
        }

        private string seoUrl(string subject)
        {
            subject = subject.Trim().ToLower();
            subject = Regex.Replace(subject, @"\s+", "-");
            subject = Regex.Replace(subject, @"[^A-Za-z0-9_-]", "");
            return subject;
        }

        //private ImageResult RenameImage()
        //{

        //}

    }
}
