﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using MovieReviews.DataAccessLayer;
using MovieReviews.Models;

namespace MovieReviews.BusinessLayer
{
    //https://www.youtube.com/watch?v=AU-4oLUV5VU
    //https://www.c-sharpcorner.com/article/custom-authentication-with-asp-net-mvc/
    //https://www.youtube.com/watch?v=EyrKUSwi4uI
    public class UserBusinessLayer
    {
        private MovieReviewsDAL movieReviewsDAL = new MovieReviewsDAL();

        public UserBusinessLayer()
        {
        }

        public string CreateSalt()
        {
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[32];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }

        public string GenerateSHA256Hash(string input, string salt)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(input + salt);
            SHA256Managed sha256hashstring = new SHA256Managed();
            byte[] hash = sha256hashstring.ComputeHash(bytes);
            return ByteArrayToHexString(hash);
        }

        public static string ByteArrayToHexString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public UserModel CheckUser(string email)
        {
            return movieReviewsDAL.Users.FirstOrDefault(u => u.email == email);
        }

        public Boolean CheckCredentials(string inputPassword, string password, string salt)
        {
            return GenerateSHA256Hash(inputPassword, salt) == password;
        }


        //public boolean CheckApiUser(string email, string password, string apiKey)
        //{
        //    return vinDAL.VinDecoderApiUserObj.Where(u => u.userEmail == email)
        //                                      .Where(u => u.password == password)
        //                                      .Where(u => u.ApiKey == apiKey).FirstOrDefault();
        //}

    }
}
