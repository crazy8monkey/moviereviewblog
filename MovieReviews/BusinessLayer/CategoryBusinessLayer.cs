﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using MovieReviews.DataAccessLayer;
using MovieReviews.Models;

namespace MovieReviews.BusinessLayer
{
    public class CategoryBusinessLayer
    {
        private MovieReviewsDAL movieReviewsDAL = new MovieReviewsDAL();

        private string seoUrl(string subject)
        {
            subject = subject.Trim().ToLower();
            subject = Regex.Replace(subject, @"\s+", "-");
            subject = Regex.Replace(subject, @"[^A-Za-z0-9_-]", "");
            return subject;
        }

        public CategoryModel NewCategory(CategoryModel category)
        {
            movieReviewsDAL.Category.Add(category);
            movieReviewsDAL.SaveChanges();
            return category;
        }

        public List<CategoryModel> CategoryList()
        {
            return movieReviewsDAL.Category.ToList();
        }

        public CategoryModel GetCategory(int categoryId)
        {
            return movieReviewsDAL.Category.Find(categoryId);
        }

        public CategoryModel GetCategoryByUrl(string seoUrl)
        {
            return movieReviewsDAL.Category.SingleOrDefault(c => c.category_seo_url == seoUrl);
        }

        public CategoryModel UpdateCategory(CategoryModel category)
        {
            var categoryToUpdate = movieReviewsDAL.Category.SingleOrDefault(c => c.category_id == category.category_id);
            categoryToUpdate.category_name = category.category_name;
            categoryToUpdate.category_seo_url = seoUrl(category.category_name);
            movieReviewsDAL.SaveChanges();
            return category;
        }

        
        public void RemoveCategory(int category_id)
        {
            var categoryToRemove = movieReviewsDAL.Category.SingleOrDefault(c => c.category_id == category_id);
            movieReviewsDAL.Category.Remove(categoryToRemove);
            movieReviewsDAL.SaveChanges();
        }

    }
}
