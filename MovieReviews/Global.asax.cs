﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using MovieReviews.Controllers;
using MovieReviews.Filters;
using MovieReviews.Models;
using Newtonsoft.Json;

namespace MovieReviews
{
    public class Global : HttpApplication
    {

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie userCookie = Request.Cookies["User"];
            if (userCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(userCookie.Value);

                var serializedUser = JsonConvert.DeserializeObject<UserModel>(authTicket.UserData);

                UserPriciple user = new UserPriciple(authTicket.Name);

                user.user_id = serializedUser.user_id;
                //principal.FirstName = serializeModel.FirstName;
                //principal.LastName = serializeModel.LastName;
                //principal.Roles = serializeModel.RoleName.ToArray<string>();

                HttpContext.Current.User = user;
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Console.WriteLine("sender => {0}", sender);
            Console.WriteLine("event args => {0}", e);
            // Do whatever you want to do with the error

            //Show the custom error page...
            //Server.ClearError();
            //var routeData = new RouteData();
            //routeData.Values["controller"] = "Error";

            //if ((Context.Server.GetLastError() is HttpException) && ((Context.Server.GetLastError() as HttpException).GetHttpCode() != 404))
            //{
            //    routeData.Values["action"] = "Index";
            //}
            //else
            //{
            //    // Handle 404 error and response code
            //    Response.StatusCode = 404;
            //    routeData.Values["action"] = "NotFound";
            //}
            //Response.TrySkipIisCustomErrors = true; // If you are using IIS7, have this line
            //IController errorsController = new ErrorController();
            //HttpContextWrapper wrapper = new HttpContextWrapper(Context);
            //var rc = new System.Web.Routing.RequestContext(wrapper, routeData);
            //errorsController.Execute(rc);

            //Response.End();
        }
    }
}
