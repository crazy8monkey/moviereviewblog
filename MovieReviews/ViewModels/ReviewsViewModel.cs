﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MovieReviews.ViewModels
{
    public class ReviewsViewModel
    {
        public int review_id { get; set; }
        public string review_title { get; set; }
        [AllowHtml]
        public string body { get; set; }
        public int is_public { get; set; }
        public string schema { get; set; }
        public string seo_url { get; set; }
        public int acting { get; set; }
        public int writing { get; set; }
        public int cast { get; set; }
        public int creativity { get; set; }
        public string movie_director { get; set; }
        public string movie_released { get; set; }
        public string movie_image { get; set; }
        public string categories_string { get; set; }
        public List<CategoryViewModel> categories { get; set; }
    }
}
