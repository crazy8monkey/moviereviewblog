﻿using System;
namespace MovieReviews.ViewModels
{
    public class CategoryViewModel
    {
        public int category_id { get; set; }
        public string category_name { get; set; }
        public bool category_checked {get; set; }
        public string category_seo_url { get; set; }
    }
}
