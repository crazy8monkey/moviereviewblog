﻿using System;
using System.Collections.Generic;

namespace MovieReviews.ViewModels
{
    public class CategoryListViewModel
    {
        public List<CategoryViewModel> CategoryList { get; set; }
    }
}
