﻿using System;
namespace MovieReviews.ViewModels
{
    public class ReviewCategoryViewModel
    {
        public int review_id { get; set; }
        public int category_id { get; set; }
    }
}
