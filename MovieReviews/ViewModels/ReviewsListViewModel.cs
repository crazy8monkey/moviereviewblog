﻿using System;
using System.Collections.Generic;

namespace MovieReviews.ViewModels
{
    public class ReviewsListViewModel
    {
        public List<ReviewsViewModel> reviews { get; set; }
        public List<String> schema { get; set; }
    }
}
